//
//  main.swift
//  SwiftPerformanceTest
//
//  Created by Philipp Hunger on 12.04.15.
//  Copyright (c) 2015 Philipp Hunger. All rights reserved.
//

import Foundation

let kNumber = 40
let kMaxIterations = 100

class PerformanceTest {
    class func fib(n: Int) -> Int {
        if (n == 0) {
            return 0
        } else if (n == 1) {
            return 1
        } else {
            return fib(n - 1) + fib(n - 2)
        }
    }
    
    class func deserializeJSON(data: NSData!) {
        NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSArray
    }
}


println("Fibonacci-Test")
var totalElapsedTime: Double = 0
var averageElapsedTime: Double
var date: NSDate!
var result: Int!
for i in 1...kMaxIterations {
    date = NSDate()
    result = PerformanceTest.fib(kNumber)
    totalElapsedTime += round(date.timeIntervalSinceNow * -1000)
}
averageElapsedTime = totalElapsedTime / Double(kMaxIterations)
println("\tfib(\(kNumber)) in \(averageElapsedTime) ms")

println("JSONTest-Test")
date = NSDate()
let data = NSData(contentsOfFile: "/Users/philipphunger/testdata.json")!
println("\tFile red in \(round(date.timeIntervalSinceNow * -1000))")
totalElapsedTime = 0
date = NSDate()
for i in 1...kMaxIterations {
    PerformanceTest.deserializeJSON(data)
    totalElapsedTime += round(date.timeIntervalSinceNow * -1000)
}
averageElapsedTime = totalElapsedTime / Double(kMaxIterations)
println("\tJson data deserialized in \(averageElapsedTime) ms")